-- bnerin npc loralen sm jaina biar kgk statusnya dead n gk agro

UPDATE `creature_template` SET `dynamicflags`=8 WHERE `entry`=37779 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=2 WHERE `entry`=37779 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=2 WHERE `entry`=37221 LIMIT 1;

-- falric sm marwyn blm ada d default db world
DELETE FROM `creature` WHERE `id` = 38112;
DELETE FROM `creature` WHERE `id` = 38113;

SET @GUID :=300000;
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (@GUID+0, 38112, 668, 1, 1, 0, 1866, 5283.75, 2030.99, 709.319, 5.57907, 300, 0, 0, 377468, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (@GUID+1, 38112, 668, 2, 1, 0, 1866, 5283.75, 2030.99, 709.319, 5.57907, 300, 0, 0, 633607, 0, 0, 0, 0, 0, 0);

INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (@GUID+2, 38113, 668, 1, 1, 0, 1867, 5334.95, 1982.31, 709.319, 2.40606, 300, 0, 0, 539240, 0, 0, 0, 0, 0, 0);
INSERT INTO `creature` (`guid`, `id`, `map`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `DeathState`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES (@GUID+3, 38113, 668, 2, 1, 0, 1867, 5334.95, 1982.31, 709.319, 2.40606, 300, 0, 0, 903227, 0, 0, 0, 0, 0, 0);

-- apus uther sm lk mereka di spawn d script instance bkn dr db

DELETE FROM `creature` WHERE `id` = 37726;
DELETE FROM `creature` WHERE `id` = 37225;
DELETE FROM `creature` WHERE `id` = 37226;

-- set flag npc trigger biar bner jadi invis plus kgk agro

UPDATE `creature_template` SET `flags_extra`=130 WHERE `entry`=37906 LIMIT 1;
UPDATE `creature_template` SET `flags_extra`=130 WHERE `entry`=37704 LIMIT 1;

-- fix TM biar bisa d attack
UPDATE `creature_template` SET `type_flags`=0, `unit_flags`=0 WHERE `entry`=38177 ;
UPDATE `creature_template` SET `type_flags`=0, `unit_flags`=0 WHERE `entry`=38173 ;
UPDATE `creature_template` SET `type_flags`=0, `unit_flags`=0 WHERE `entry`=38176 ;
UPDATE `creature_template` SET `type_flags`=0, `unit_flags`=0 WHERE `entry`=38175 ;
UPDATE `creature_template` SET `type_flags`=0, `unit_flags`=0 WHERE `entry`=38172 ;

